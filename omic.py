import numpy as np
import scipy as scipy
from scipy.sparse import csr_matrix, random
import itertools
import random
import com
from matrix import SparseMatrix

class Omic:
    def __init__(self, k, l, R, mat, epochs):
        self.k = k
        self.l = l
        self.R = R
        self.mat = mat
        self.epochs = epochs
        self.m = mat.shape[0]
        self.n = mat.shape[1]

    @staticmethod
    def complement(m):
        X = np.array(np.repeat(0.0, m * (m - 1))).reshape(m, m - 1)
        for i in range((m - 2)):
            v = X[:, i]
            v[0:i] = 0
            v[i] = 1
            v[(i + 1):m] = (-1 / (m - i - 1))
            X[:, i] = v / np.linalg.norm(v)
        v = X[:, m - 2]
        v[m - 2] = 1
        v[m - 1] = -1
        X[:, m - 2] = v / np.linalg.norm(v)
        return X

    @staticmethod
    def complementVector(v):
        x = v.copy()
        m = v.shape[0]
        comp = Omic.complement(np.sum(x != 0))
        Xcomp = np.array(np.repeat(0.0, m * (comp.shape[1]))).reshape(m, comp.shape[1])
        Xcomp[x != 0, :] = comp
        return Xcomp

    @staticmethod
    def complementBias(v):
        mx = int(np.max(v))
        cls = np.repeat(0.0, mx + 1)
        for i in range(1, (mx + 1)):
            cls[i] = np.sum(v == i)
        m = len(v)
        X = np.array(np.repeat(0.0, m * (mx - 1))).reshape(m, mx - 1)
        for i in range(1, (mx)):
            cv = np.concatenate((np.repeat(0.0, np.sum(cls[range(0, i)])), np.repeat(1 / cls[i], cls[i])), axis=None)
            cv = np.concatenate((cv, np.repeat((-1 / (np.sum(cls[(i + 1):(mx + 1)]))), np.sum(cls[(i + 1):(mx + 1)]))),
                                axis=None)
            X[:, i - 1] = np.transpose((cv / np.linalg.norm(cv))[np.newaxis])
        return X

    @staticmethod
    def GETXs(m):
        X1 = np.repeat(1 / np.sqrt(m), m)
        X2 = Omic.complementVector(X1)
        return [X1, X2]

    @staticmethod
    def svd_thresholding(m_tilde, l):
        print(m_tilde)
        u, d, v = np.linalg.svd(m_tilde, full_matrices=False)

        d_tilde = np.diag(d - l)
        d_tilde = np.diag(d_tilde[np.diag(d - 1 >= 0)])
        u_tilde = u[:, range(d_tilde.shape[0])]
        v_tilde = v[:d_tilde.shape[0], :]

        m_new = np.dot(u_tilde @ d_tilde, v_tilde)
        return m_new

    def inducive_matrix_completion(self):
        lam_list = np.linspace(0, 1, self.k * self.l, endpoint=False)
        print(lam_list)
        # self.lambdas = np.array(lam_list).reshape(self.k, self.l)
        self.lambdas = np.array([0, 0.1, 0.2, 0.5]).reshape(self.k, self.l)
        xs = Omic.GETXs(self.m)
        ys = Omic.GETXs(self.n)
        xs[0] = xs[0].reshape(-1, 1)
        ys[0] = ys[0].reshape(-1, 1)
        self.xs = xs
        self.ys = ys
        r = self.mat.copy()
        for a in range(self.epochs):
            nr = None
            for i in range(self.k):
                for j in range(self.l):
                    print("\ni = ", i, "j = ", j)
                    xry = np.dot(xs[i].T @ r, ys[j])
                    m_thres = Omic.svd_thresholding(xry, self.lambdas[i][j])
                    # mat[i][j].append(m_thres.copy())
                    print(xs[i].shape, m_thres.shape, ys[j].T.shape)
                    if nr is None:
                        nr = np.dot(xs[i], m_thres @ ys[j].T)
                    else:
                        print(nr.shape)
                        nr = np.add(nr, np.dot(xs[i], m_thres @ ys[j].T))
            print(nr.shape)
            row_omega, col_omega = zip(*self.R.idx_for_training)
            nr[row_omega, col_omega] = r[row_omega, col_omega]
            r = nr
        return r


if __name__ == "__main__":
    m = 100
    n = 100
    rank = 3
    train_val_test_split = [0.85, 0.10, 0.05]
    R = SparseMatrix(m, n, rank, train_val_test_split)
    print("Rank of X =", np.linalg.matrix_rank(R.generated_matrix), "\nShape of X =", R.generated_matrix.shape)
    print("Num of ~NaN in obs mat :", np.where(~np.isnan(R.observed_matrix))[0].shape)
    print("Num of ~NaN in train mat :", np.where(~np.isnan(R.train))[0].shape)
    print("Num of ~NaN in val mat :", np.where(~np.isnan(R.val))[0].shape)
    print("Num of ~NaN in test mat :", np.where(~np.isnan(R.test))[0].shape)
    k, l = 2, 2
    epochs = 1
    mat = R.train
    print(len(R.idx_not_train))
    rows, cols = zip(*(R.idx_not_train + R.nan_idx_list))
    mat[rows, cols] = 0
    omic = Omic(k, l, R, mat, epochs)
    result = omic.inducive_matrix_completion()
    print(result)
    diff = [result[i][j] - mat[i][j] for i, j in zip(rows, cols)]
    print('-----The MSE is : ', np.sum(np.square(diff)).mean())