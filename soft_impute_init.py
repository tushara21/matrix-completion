import numpy as np
import scipy as scipy
from scipy.sparse import csr_matrix, random
import itertools
import random


def soft_impute(x_tilde, z_old, l, row_list, col_list):
    z_tilde = z_old.copy()
    z_tilde[row_list, col_list] = 0
    m_tilde = x_tilde + z_tilde
    
    u, d, v = np.linalg.svd(m_tilde, full_matrices=False)
    # print(u.shape, d.shape, v.shape)
    # print(np.allclose(m_tilde, np.dot(u * d, v)))
    
    d_tilde = np.diag(d-l)
    d_tilde = np.diag(d_tilde[np.diag(d-1 >= 0)])
    u_tilde = u[:,range(d_tilde.shape[0])]
    v_tilde = v[:d_tilde.shape[0], :]
    
    z_new = np.dot(u_tilde @ d_tilde, v_tilde)
    
    # print(u_tilde.shape, d_tilde.shape, v_tilde.shape)
    # print(z_old.shape, z_new.shape)
    return z_new


# Generation of 'generated' -> X matrix
m, n = 100, 100
rank = 3
U = np.random.randn(m, rank)
V = np.random.randn(n, rank)
x = np.matmul(U, V.T)
print("Rank of X =", np.linalg.matrix_rank(x), "\nShape of X =", x.shape)
x_ = x.copy()

# Random indices to make the matrices sparse
idx_pool = [[i for i in range(m)], [j for j in range(n)]]
idx_list = list(itertools.product(*idx_pool))
nan_idx_list = random.sample(idx_list, int(0.7*m*n))
non_nan_idx_list = list(set(idx_list) - set(nan_idx_list))

rows, cols = zip(*nan_idx_list)

# 'observed' -> X_ matrix
x_[rows, cols] = np.nan

idx_for_training = random.sample(non_nan_idx_list, int(0.9*len(non_nan_idx_list)))
idx_for_val = list(set(non_nan_idx_list) - set(idx_for_training))

# observed training matrix with 90% values intact from x_
x_train = x_.copy()
idx_r, idx_c = zip(*idx_for_val)
x_train[idx_r, idx_c] = np.nan
print(np.where(~np.isnan(x_train))[0].shape)

# observed validation matrix with 10% values intact from x_
x_val = x_.copy()
idx_r_v, idx_c_v = zip(*idx_for_training)
x_val[idx_r_v, idx_c_v] = np.nan
print(np.where(~np.isnan(x_val))[0].shape)

# x_tilde is the P_omega(x_train)
x_tilde = x_train.copy()
r_x = rows + idx_r
c_x = cols + idx_c
x_tilde[r_x, c_x] = 0

z_old = np.zeros_like(x_)
lambda_ = [0, 1, 2, 5]

# these indices will be used to create the z_tilde which is the P_omega_perp(x_train)
row_list = np.where(~np.isnan(x_train))[0].tolist()
col_list = np.where(~np.isnan(x_train))[1].tolist()

lambda_z_map = {}
for l in lambda_:
    for i in range(100):
        z_old = soft_impute(x_tilde, z_old, l, row_list, col_list)
    diff = [z_old[i][j] - x_val[i][j] for i,j in zip(idx_r, idx_c)]
    lambda_z_map[l] = np.sum(np.square(diff))

print(lambda_z_map)