import numpy as np
import scipy as scipy
from scipy.sparse import csr_matrix, random
import itertools
import random
import com

class SparseMatrix:
    def __init__(self, m, n, rank, train_val_test_split, sparsity=0.7):
        self.m = m
        self.n = n
        self.rank = rank
        self.sparsity = sparsity
        self.generated_matrix = self.get_generated_matrix()
        self.observed_matrix = self.get_observed_matrix()
        if sum(train_val_test_split) != 1:
            raise Exception("The train-val-test split should sum up to 1")
        else:
            self.train_val_test_split = train_val_test_split
            self.train, self.val, self.test = self.get_train_val_test_matrices()

    def get_generated_matrix(self):
        self.U = np.random.randn(self.m, self.rank)
        self.V = np.random.randn(self.n, self.rank)
        return np.matmul(self.U, self.V.T)

    def get_observed_matrix(self):
        self.idx_pool, self.idx_list, self.nan_idx_list, self.non_nan_idx_list = \
            SparseMatrix.get_random_sparsity_indices(self.m, self.n, self.sparsity)
        rows, cols = zip(*self.nan_idx_list)
        x_ = self.generated_matrix.copy()
        x_[rows, cols] = np.nan
        return x_

    @staticmethod
    def get_random_sparsity_indices(m, n, spar):
        idx_pool = [[i for i in range(m)], [j for j in range(n)]]
        idx_list = list(itertools.product(*idx_pool))
        nan_idx_list = random.sample(idx_list, int(spar * m * n))
        non_nan_idx_list = list(set(idx_list) - set(nan_idx_list))
        return idx_pool, idx_list, nan_idx_list, non_nan_idx_list

    def get_train_val_test_matrices(self):
        self.idx_for_training, self.idx_not_train, self.idx_for_val, self.idx_for_test = \
            SparseMatrix.get_random_train_test_val_indices(self.non_nan_idx_list, self.train_val_test_split)
        x_train, x_val, x_test = self.observed_matrix.copy(), self.observed_matrix.copy(), self.observed_matrix.copy()

        idx_r, idx_c = zip(*self.idx_not_train)
        x_train[idx_r, idx_c] = np.nan

        idx_r_v, idx_c_v = zip(*(self.idx_for_training + self.idx_for_test))
        x_val[idx_r_v, idx_c_v] = np.nan

        idx_r_t, idx_c_t = zip(*(self.idx_for_training + self.idx_for_val))
        x_test[idx_r_t, idx_c_t] = np.nan

        return x_train, x_val, x_test

    @staticmethod
    def get_random_train_test_val_indices(non_nan_idx_list, tvt):
        idx_for_training = random.sample(non_nan_idx_list, int(tvt[0] * len(non_nan_idx_list)))
        idx_not_train = list(set(non_nan_idx_list) - set(idx_for_training))
        val_sp = tvt[1] / (tvt[1] + tvt[2])
        idx_for_val = random.sample(idx_not_train, int(val_sp * len(idx_not_train)))  # 10 of 15 is 66%
        idx_for_test = list(set(idx_not_train) - set(idx_for_val))  # 5 of 15 is 33% (remaining)
        return idx_for_training, idx_not_train, idx_for_val, idx_for_test


if __name__ == "__main__":
    m = 100
    n = 100
    rank = 3
    train_val_test_split = [0.85, 0.10, 0.05]
    mat = SparseMatrix(m, n, rank, train_val_test_split)
    print("Rank of X =", np.linalg.matrix_rank(mat.generated_matrix), "\nShape of X =", mat.generated_matrix.shape)
    print("Num of ~NaN in obs mat :", np.where(~np.isnan(mat.observed_matrix))[0].shape)
    print("Num of ~NaN in train mat :", np.where(~np.isnan(mat.train))) #[0].shape)
    print("Num of ~NaN in val mat :", np.where(~np.isnan(mat.val))[0].shape)
    print("Num of ~NaN in test mat :", np.where(~np.isnan(mat.test))[0].shape)
