import numpy as np

def complement(m):
	X = np.matrix(np.repeat(0.0, m * (m-1))).reshape(m,m-1)
	for i in range((m-2)):
		v = X[:,i]
		v[0:i] = 0
		v[i] = 1
		v[(i+1):m] = (-1/(m-i-1))
		X[:,i]=v/np.linalg.norm(v)
	v = X[:,m-2]
	v[m-2]=1
	v[m-1]=-1
	X[:,m-2]=v/np.linalg.norm(v)
	return X

def complementVector(v):
	x = v.copy()
	m=v.shape[0]
	comp = complement(np.sum(x!=0))
	Xcomp = np.matrix(np.repeat(0.0, m * (comp.shape[1]))).reshape(m,comp.shape[1])
	Xcomp[x!=0,:] = comp
	return Xcomp

def complementBias(v):
	mx = int(np.max(v))
	cls = np.repeat(0.0,mx+1)
	for i in range(1,(mx+1)):
		cls[i] = np.sum(v==i)
	m = len(v)
	X = np.matrix(np.repeat(0.0, m * (mx-1))).reshape(m,mx-1)
	for i in range(1,(mx)):
		cv = np.concatenate((np.repeat(0.0,np.sum(cls[range(0,i)])) , np.repeat(1/cls[i],cls[i])),axis=None)
		cv = np.concatenate((cv , np.repeat((-1 / (np.sum(cls[(i+1):(mx+1)]))),np.sum(cls[(i+1):(mx+1)]))),axis=None)
		X[:,i-1]=np.transpose((cv/np.linalg.norm(cv))[np.newaxis])
		
	return X
	
def GETXs(m):
	X1 = np.repeat(1/np.sqrt(m), m) 
	X2 = complementVector(X1)
	return [X1, X2]
	